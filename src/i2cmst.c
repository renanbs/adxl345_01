/****************************************************************************
 *   $Id:: i2cmst.c 6097 2011-01-07 04:31:25Z nxp12832                      $
 *   Project: NXP LPC17xx I2C example
 *
 *   Description:
 *     This file contains I2C test modules, main entry, to test I2C APIs.
 *
 ****************************************************************************
 * Software that is described herein is for illustrative purposes only
 * which provides customers with programming information regarding the
 * products. This software is supplied "AS IS" without any warranties.
 * NXP Semiconductors assumes no responsibility or liability for the
 * use of the software, conveys no license or title under any patent,
 * copyright, or mask work right to the product. NXP Semiconductors
 * reserves the right to make changes in the software without
 * notification. NXP Semiconductors also make no representation or
 * warranty that such application will be suitable for the specified
 * use without further testing or modification.
****************************************************************************/
#include <cr_section_macros.h>
#include <NXP/crp.h>

// Variable to store CRP value in. Will be placed automatically
// by the linker when "Enable Code Read Protect" selected.
// See crp.h header for more information
__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

#include "LPC17xx.h"
#include "types.h"
#include "i2c.h"
#include "uart0.h"
#include "XL345.h"

#include "adxl345.h"	// Lib to comunicate with adxl345

#include <string.h>
#include <stdio.h>
#include <stdlib.h>



volatile uint32_t msTicks; // counter for 1ms SysTicks

// ****************
//  SysTick_Handler - just increment SysTick counter
void SysTick_Handler(void) {
  msTicks++;
  adxl345_timering();
}

// ****************
// ms_delay - creates a delay of the appropriate number of Systicks (happens every 1 ms)
__INLINE static void ms_delay (uint32_t delayTicks) {
  uint32_t currentTicks;

  currentTicks = msTicks;	// read current tick counter
  // Now loop until required number of ticks passes.
  while ((msTicks - currentTicks) < delayTicks);
}

short x;
short y;
short z;

//static void gpioIntInit(void)
//{
//	LPC_PINCON->PINSEL0    &= ~(1<<8);//|(3<<12)|(3<<14)); //set p0.0/6/7 to GPIO (00) function (button connection)
//	LPC_GPIO0->FIODIR      &= ~(1 << 8);                 //set key pins to input (0)
//	LPC_PINCON->PINMODE0   |=  (1<<8);//|(3<<12)|(3<<14)); //pull down
//
//	//Enable rising edge interrupt for P0.8
//	LPC_GPIOINT->IO0IntEnR |= 1 << 8;//| 1<<9;
//	//LPC_GPIOINT->IO0IntEnF |= 1 << 8;
////	NVIC_SetPriority(EINT3_IRQn, 30);
//	NVIC_EnableIRQ(EINT3_IRQn);
//}
//
//void EINT3_IRQHandler(void)
//{
//	unsigned long regVal;
//	regVal = LPC_GPIOINT->IntStatus;
//
//	if ((LPC_GPIOINT->IO0IntStatR & (1 << 8)) == (1 << 8)) // GPIO 0.8 rising edge?
//	{
//		LPC_GPIOINT->IO0IntClr = 1 << 8;
//		UART0_PrintString ("Rising edge..\n\r");
//		if (adxl345_xyz (&x, &y, &z))
//		{
//			char msg[50];
//			sprintf (msg, "x = 0x%X(%d), y = 0x%X(%d), z = 0x%X(%d)\r", x, x, y, y, z, z);
//			UART0_PrintString (msg);
//		}
//
//	}
//
//	if ((LPC_GPIOINT->IO0IntStatF & (1 << 8)) == (1 << 8)) // GPIO 0.8 falling edge?
//	{
//		LPC_GPIOINT->IO0IntClr = 1 << 8;
//		UART0_PrintString ("Falling edge..\n\r");
//	}
//	return;
//}

/*******************************************************************************
**   Main Function  main()
*******************************************************************************/
int main (void)
{
//	char msg[50];
	// SystemClockUpdate() updates the SystemFrequency variable
	SystemCoreClockUpdate();

	UART0_Init(115200);
	UART0_PrintString("Uart0 initialized!\r\n");

	// Setup SysTick Timer to interrupt at 1 msec intervals
	if (SysTick_Config(SystemCoreClock / 1000)) {
	    while (1);  // Capture error
	}

	if ( I2CInit() == FALSE )	/* initialize I2c */
	{
		UART0_PrintString ("Could not initialize I2C0....\r\n");
		while ( 1 );				/* Fatal error */
	}

	UART0_PrintString ("Trying to initialize adxl 345...\r\n");

	adxl345_init_alt ();
//	adxl345_init ();

	char msg[50];

	while (1)
	{
		adxl345_xyz (&x, &y, &z);
		memset (msg, 0x00, sizeof (msg));
		sprintf (msg, "x = 0x%X(%d), y = 0x%X(%d), z = 0x%X(%d)\r", x, x, y, y, z, z);
		UART0_PrintString (msg);

		ms_delay (500);
	}

	return 0;
}

/******************************************************************************
**                            End Of File
******************************************************************************/
