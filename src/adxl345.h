/*
 * adxl345.h
 *
 *  Created on: May 7, 2012
 *      Author: renan
 */

#ifndef ADXL345_H_
#define ADXL345_H_

#include "LPC17xx.h"
#include "types.h"
#include "i2c.h"
#include "uart0.h"
#include "XL345.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

//extern volatile uint8_t I2CMasterBuffer[BUFSIZE];
//extern volatile uint8_t I2CSlaveBuffer[BUFSIZE];
//extern volatile uint32_t I2CReadLength;
//extern volatile uint32_t I2CWriteLength;

void adxl345_timering ();
int adxl345_write_byte (uint8_t reg, uint8_t data, uint8_t read_len);
int adxl345_read_byte (uint8_t len);
int adxl345_init ();
int adxl345_init_alt ();
int adxl345_fifo_mode (uint8_t mode);
int adxl345_device_id (unsigned char *dev_id);
int adxl345_xyz (volatile short *x, volatile short *y, volatile short *z);//(short *x, short *y, short *z);



#endif /* ADXL345_H_ */
